# Generated from main/mp/parser/MP.g4 by ANTLR 4.7.1
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


from lexererr import *


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2=")
        buf.write("\u0223\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36")
        buf.write("\t\36\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%")
        buf.write("\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4,\t,\4-\t-\4.")
        buf.write("\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64")
        buf.write("\t\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:")
        buf.write("\4;\t;\4<\t<\4=\t=\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\t")
        buf.write("C\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I\tI\4J\tJ\4K\tK\4L\t")
        buf.write("L\4M\tM\4N\tN\4O\tO\4P\tP\4Q\tQ\4R\tR\4S\tS\4T\tT\4U\t")
        buf.write("U\4V\tV\4W\tW\4X\tX\4Y\tY\3\2\3\2\3\2\3\2\3\2\3\3\3\3")
        buf.write("\3\4\3\4\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\t\3")
        buf.write("\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3")
        buf.write("\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\17")
        buf.write("\3\17\3\17\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\21\3\21")
        buf.write("\3\21\3\22\3\22\3\22\3\22\3\22\3\23\3\23\3\23\3\23\3\23")
        buf.write("\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\25\3\25\3\25\3\25")
        buf.write("\3\25\3\25\3\26\3\26\3\26\3\26\3\26\3\26\3\27\3\27\3\27")
        buf.write("\3\27\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\31")
        buf.write("\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\32\3\32")
        buf.write("\3\32\3\32\3\33\3\33\3\33\3\33\3\33\3\34\3\34\3\34\3\34")
        buf.write("\3\34\3\34\3\35\3\35\3\35\3\35\3\35\3\35\3\36\3\36\3\36")
        buf.write("\3\37\3\37\3\37\3\37\3\37\3 \3 \3 \3 \3 \3 \3 \3 \3!\3")
        buf.write("!\3!\3!\3!\3!\3!\3!\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3#\3#")
        buf.write("\3#\3#\3$\3$\3$\3$\3%\3%\3%\3&\3&\3&\3&\3\'\3\'\3\'\3")
        buf.write("\'\3(\3(\3)\3)\3*\3*\3+\3+\3,\3,\3-\3-\3.\3.\3/\3/\3\60")
        buf.write("\3\60\3\61\3\61\3\62\3\62\3\63\3\63\3\64\3\64\3\65\3\65")
        buf.write("\3\66\3\66\3\67\3\67\38\38\39\39\3:\3:\3;\3;\3<\3<\3=")
        buf.write("\3=\3>\3>\3?\3?\3@\3@\3A\3A\3B\3B\3C\3C\3D\3D\3E\3E\3")
        buf.write("F\3F\3G\3G\3H\3H\3H\3I\3I\3I\3J\3J\3K\3K\3K\3L\6L\u01b4")
        buf.write("\nL\rL\16L\u01b5\3M\6M\u01b9\nM\rM\16M\u01ba\3M\3M\6M")
        buf.write("\u01bf\nM\rM\16M\u01c0\3M\5M\u01c4\nM\3M\3M\5M\u01c8\n")
        buf.write("M\3M\3M\6M\u01cc\nM\rM\16M\u01cd\3M\5M\u01d1\nM\5M\u01d3")
        buf.write("\nM\3N\3N\7N\u01d7\nN\fN\16N\u01da\13N\3N\3N\3O\3O\3O")
        buf.write("\7O\u01e1\nO\fO\16O\u01e4\13O\3P\3P\3Q\3Q\3R\3R\5R\u01ec")
        buf.write("\nR\3R\6R\u01ef\nR\rR\16R\u01f0\3S\3S\3S\3S\7S\u01f7\n")
        buf.write("S\fS\16S\u01fa\13S\3S\3S\3S\3S\3S\3T\3T\7T\u0203\nT\f")
        buf.write("T\16T\u0206\13T\3T\3T\3T\3T\3U\3U\3U\3U\7U\u0210\nU\f")
        buf.write("U\16U\u0213\13U\3U\3U\3V\6V\u0218\nV\rV\16V\u0219\3V\3")
        buf.write("V\3W\3W\3X\3X\3Y\3Y\5\u01d8\u01f8\u0204\2Z\3\3\5\4\7\5")
        buf.write("\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35")
        buf.write("\20\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33")
        buf.write("\65\34\67\359\36;\37= ?!A\"C#E$G%I&K\'M(O\2Q\2S\2U\2W")
        buf.write("\2Y\2[\2]\2_\2a\2c\2e\2g\2i\2k\2m\2o\2q\2s\2u\2w\2y\2")
        buf.write("{\2}\2\177\2\u0081\2\u0083)\u0085*\u0087+\u0089,\u008b")
        buf.write("-\u008d.\u008f/\u0091\60\u0093\61\u0095\62\u0097\63\u0099")
        buf.write("\64\u009b\65\u009d\66\u009f\2\u00a1\2\u00a3\2\u00a5\67")
        buf.write("\u00a78\u00a99\u00ab:\u00ad;\u00af<\u00b1=\3\2!\4\2CC")
        buf.write("cc\4\2DDdd\4\2EEee\4\2FFff\4\2GGgg\4\2HHhh\4\2IIii\4\2")
        buf.write("JJjj\4\2KKkk\4\2LLll\4\2MMmm\4\2NNnn\4\2OOoo\4\2PPpp\4")
        buf.write("\2QQqq\4\2RRrr\4\2SSss\4\2TTtt\4\2UUuu\4\2VVvv\4\2WWw")
        buf.write("w\4\2XXxx\4\2YYyy\4\2ZZzz\4\2[[{{\4\2\\\\||\5\2C\\aac")
        buf.write("|\3\2\62;\4\2--//\4\2\f\f\17\17\5\2\13\f\17\17\"\"\2\u0217")
        buf.write("\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13")
        buf.write("\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3")
        buf.write("\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2")
        buf.write("\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2")
        buf.write("%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2")
        buf.write("\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67")
        buf.write("\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2")
        buf.write("A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2")
        buf.write("\2K\3\2\2\2\2M\3\2\2\2\2\u0083\3\2\2\2\2\u0085\3\2\2\2")
        buf.write("\2\u0087\3\2\2\2\2\u0089\3\2\2\2\2\u008b\3\2\2\2\2\u008d")
        buf.write("\3\2\2\2\2\u008f\3\2\2\2\2\u0091\3\2\2\2\2\u0093\3\2\2")
        buf.write("\2\2\u0095\3\2\2\2\2\u0097\3\2\2\2\2\u0099\3\2\2\2\2\u009b")
        buf.write("\3\2\2\2\2\u009d\3\2\2\2\2\u00a5\3\2\2\2\2\u00a7\3\2\2")
        buf.write("\2\2\u00a9\3\2\2\2\2\u00ab\3\2\2\2\2\u00ad\3\2\2\2\2\u00af")
        buf.write("\3\2\2\2\2\u00b1\3\2\2\2\3\u00b3\3\2\2\2\5\u00b8\3\2\2")
        buf.write("\2\7\u00ba\3\2\2\2\t\u00bc\3\2\2\2\13\u00be\3\2\2\2\r")
        buf.write("\u00c0\3\2\2\2\17\u00c2\3\2\2\2\21\u00c4\3\2\2\2\23\u00c7")
        buf.write("\3\2\2\2\25\u00c9\3\2\2\2\27\u00cf\3\2\2\2\31\u00d8\3")
        buf.write("\2\2\2\33\u00dc\3\2\2\2\35\u00df\3\2\2\2\37\u00e6\3\2")
        buf.write("\2\2!\u00e9\3\2\2\2#\u00ec\3\2\2\2%\u00f1\3\2\2\2\'\u00f6")
        buf.write("\3\2\2\2)\u00fd\3\2\2\2+\u0103\3\2\2\2-\u0109\3\2\2\2")
        buf.write("/\u010d\3\2\2\2\61\u0116\3\2\2\2\63\u0120\3\2\2\2\65\u0124")
        buf.write("\3\2\2\2\67\u0129\3\2\2\29\u012f\3\2\2\2;\u0135\3\2\2")
        buf.write("\2=\u0138\3\2\2\2?\u013d\3\2\2\2A\u0145\3\2\2\2C\u014d")
        buf.write("\3\2\2\2E\u0154\3\2\2\2G\u0158\3\2\2\2I\u015c\3\2\2\2")
        buf.write("K\u015f\3\2\2\2M\u0163\3\2\2\2O\u0167\3\2\2\2Q\u0169\3")
        buf.write("\2\2\2S\u016b\3\2\2\2U\u016d\3\2\2\2W\u016f\3\2\2\2Y\u0171")
        buf.write("\3\2\2\2[\u0173\3\2\2\2]\u0175\3\2\2\2_\u0177\3\2\2\2")
        buf.write("a\u0179\3\2\2\2c\u017b\3\2\2\2e\u017d\3\2\2\2g\u017f\3")
        buf.write("\2\2\2i\u0181\3\2\2\2k\u0183\3\2\2\2m\u0185\3\2\2\2o\u0187")
        buf.write("\3\2\2\2q\u0189\3\2\2\2s\u018b\3\2\2\2u\u018d\3\2\2\2")
        buf.write("w\u018f\3\2\2\2y\u0191\3\2\2\2{\u0193\3\2\2\2}\u0195\3")
        buf.write("\2\2\2\177\u0197\3\2\2\2\u0081\u0199\3\2\2\2\u0083\u019b")
        buf.write("\3\2\2\2\u0085\u019d\3\2\2\2\u0087\u019f\3\2\2\2\u0089")
        buf.write("\u01a1\3\2\2\2\u008b\u01a3\3\2\2\2\u008d\u01a5\3\2\2\2")
        buf.write("\u008f\u01a7\3\2\2\2\u0091\u01aa\3\2\2\2\u0093\u01ad\3")
        buf.write("\2\2\2\u0095\u01af\3\2\2\2\u0097\u01b3\3\2\2\2\u0099\u01d2")
        buf.write("\3\2\2\2\u009b\u01d4\3\2\2\2\u009d\u01dd\3\2\2\2\u009f")
        buf.write("\u01e5\3\2\2\2\u00a1\u01e7\3\2\2\2\u00a3\u01e9\3\2\2\2")
        buf.write("\u00a5\u01f2\3\2\2\2\u00a7\u0200\3\2\2\2\u00a9\u020b\3")
        buf.write("\2\2\2\u00ab\u0217\3\2\2\2\u00ad\u021d\3\2\2\2\u00af\u021f")
        buf.write("\3\2\2\2\u00b1\u0221\3\2\2\2\u00b3\u00b4\5y=\2\u00b4\u00b5")
        buf.write("\5k\66\2\u00b5\u00b6\5_\60\2\u00b6\u00b7\5U+\2\u00b7\4")
        buf.write("\3\2\2\2\u00b8\u00b9\7*\2\2\u00b9\6\3\2\2\2\u00ba\u00bb")
        buf.write("\7+\2\2\u00bb\b\3\2\2\2\u00bc\u00bd\7]\2\2\u00bd\n\3\2")
        buf.write("\2\2\u00be\u00bf\7_\2\2\u00bf\f\3\2\2\2\u00c0\u00c1\7")
        buf.write("<\2\2\u00c1\16\3\2\2\2\u00c2\u00c3\7=\2\2\u00c3\20\3\2")
        buf.write("\2\2\u00c4\u00c5\7\60\2\2\u00c5\u00c6\7\60\2\2\u00c6\22")
        buf.write("\3\2\2\2\u00c7\u00c8\7.\2\2\u00c8\24\3\2\2\2\u00c9\u00ca")
        buf.write("\5Q)\2\u00ca\u00cb\5q9\2\u00cb\u00cc\5W,\2\u00cc\u00cd")
        buf.write("\5O(\2\u00cd\u00ce\5c\62\2\u00ce\26\3\2\2\2\u00cf\u00d0")
        buf.write("\5S*\2\u00d0\u00d1\5k\66\2\u00d1\u00d2\5i\65\2\u00d2\u00d3")
        buf.write("\5u;\2\u00d3\u00d4\5_\60\2\u00d4\u00d5\5i\65\2\u00d5\u00d6")
        buf.write("\5w<\2\u00d6\u00d7\5W,\2\u00d7\30\3\2\2\2\u00d8\u00d9")
        buf.write("\5Y-\2\u00d9\u00da\5k\66\2\u00da\u00db\5q9\2\u00db\32")
        buf.write("\3\2\2\2\u00dc\u00dd\5u;\2\u00dd\u00de\5k\66\2\u00de\34")
        buf.write("\3\2\2\2\u00df\u00e0\5U+\2\u00e0\u00e1\5k\66\2\u00e1\u00e2")
        buf.write("\5{>\2\u00e2\u00e3\5i\65\2\u00e3\u00e4\5u;\2\u00e4\u00e5")
        buf.write("\5k\66\2\u00e5\36\3\2\2\2\u00e6\u00e7\5U+\2\u00e7\u00e8")
        buf.write("\5k\66\2\u00e8 \3\2\2\2\u00e9\u00ea\5_\60\2\u00ea\u00eb")
        buf.write("\5Y-\2\u00eb\"\3\2\2\2\u00ec\u00ed\5u;\2\u00ed\u00ee\5")
        buf.write("]/\2\u00ee\u00ef\5W,\2\u00ef\u00f0\5i\65\2\u00f0$\3\2")
        buf.write("\2\2\u00f1\u00f2\5W,\2\u00f2\u00f3\5e\63\2\u00f3\u00f4")
        buf.write("\5s:\2\u00f4\u00f5\5W,\2\u00f5&\3\2\2\2\u00f6\u00f7\5")
        buf.write("q9\2\u00f7\u00f8\5W,\2\u00f8\u00f9\5u;\2\u00f9\u00fa\5")
        buf.write("w<\2\u00fa\u00fb\5q9\2\u00fb\u00fc\5i\65\2\u00fc(\3\2")
        buf.write("\2\2\u00fd\u00fe\5{>\2\u00fe\u00ff\5]/\2\u00ff\u0100\5")
        buf.write("_\60\2\u0100\u0101\5e\63\2\u0101\u0102\5W,\2\u0102*\3")
        buf.write("\2\2\2\u0103\u0104\5Q)\2\u0104\u0105\5W,\2\u0105\u0106")
        buf.write("\5[.\2\u0106\u0107\5_\60\2\u0107\u0108\5i\65\2\u0108,")
        buf.write("\3\2\2\2\u0109\u010a\5W,\2\u010a\u010b\5i\65\2\u010b\u010c")
        buf.write("\5U+\2\u010c.\3\2\2\2\u010d\u010e\5Y-\2\u010e\u010f\5")
        buf.write("w<\2\u010f\u0110\5i\65\2\u0110\u0111\5S*\2\u0111\u0112")
        buf.write("\5u;\2\u0112\u0113\5_\60\2\u0113\u0114\5k\66\2\u0114\u0115")
        buf.write("\5i\65\2\u0115\60\3\2\2\2\u0116\u0117\5m\67\2\u0117\u0118")
        buf.write("\5q9\2\u0118\u0119\5k\66\2\u0119\u011a\5S*\2\u011a\u011b")
        buf.write("\5W,\2\u011b\u011c\5U+\2\u011c\u011d\5w<\2\u011d\u011e")
        buf.write("\5q9\2\u011e\u011f\5W,\2\u011f\62\3\2\2\2\u0120\u0121")
        buf.write("\5y=\2\u0121\u0122\5O(\2\u0122\u0123\5q9\2\u0123\64\3")
        buf.write("\2\2\2\u0124\u0125\5u;\2\u0125\u0126\5q9\2\u0126\u0127")
        buf.write("\5w<\2\u0127\u0128\5W,\2\u0128\66\3\2\2\2\u0129\u012a")
        buf.write("\5Y-\2\u012a\u012b\5O(\2\u012b\u012c\5e\63\2\u012c\u012d")
        buf.write("\5s:\2\u012d\u012e\5W,\2\u012e8\3\2\2\2\u012f\u0130\5")
        buf.write("O(\2\u0130\u0131\5q9\2\u0131\u0132\5q9\2\u0132\u0133\5")
        buf.write("O(\2\u0133\u0134\5\177@\2\u0134:\3\2\2\2\u0135\u0136\5")
        buf.write("k\66\2\u0136\u0137\5Y-\2\u0137<\3\2\2\2\u0138\u0139\5")
        buf.write("q9\2\u0139\u013a\5W,\2\u013a\u013b\5O(\2\u013b\u013c\5")
        buf.write("e\63\2\u013c>\3\2\2\2\u013d\u013e\5Q)\2\u013e\u013f\5")
        buf.write("k\66\2\u013f\u0140\5k\66\2\u0140\u0141\5e\63\2\u0141\u0142")
        buf.write("\5W,\2\u0142\u0143\5O(\2\u0143\u0144\5i\65\2\u0144@\3")
        buf.write("\2\2\2\u0145\u0146\5_\60\2\u0146\u0147\5i\65\2\u0147\u0148")
        buf.write("\5u;\2\u0148\u0149\5W,\2\u0149\u014a\5[.\2\u014a\u014b")
        buf.write("\5W,\2\u014b\u014c\5q9\2\u014cB\3\2\2\2\u014d\u014e\5")
        buf.write("s:\2\u014e\u014f\5u;\2\u014f\u0150\5q9\2\u0150\u0151\5")
        buf.write("_\60\2\u0151\u0152\5i\65\2\u0152\u0153\5[.\2\u0153D\3")
        buf.write("\2\2\2\u0154\u0155\5i\65\2\u0155\u0156\5k\66\2\u0156\u0157")
        buf.write("\5u;\2\u0157F\3\2\2\2\u0158\u0159\5O(\2\u0159\u015a\5")
        buf.write("i\65\2\u015a\u015b\5U+\2\u015bH\3\2\2\2\u015c\u015d\5")
        buf.write("k\66\2\u015d\u015e\5q9\2\u015eJ\3\2\2\2\u015f\u0160\5")
        buf.write("U+\2\u0160\u0161\5_\60\2\u0161\u0162\5y=\2\u0162L\3\2")
        buf.write("\2\2\u0163\u0164\5g\64\2\u0164\u0165\5k\66\2\u0165\u0166")
        buf.write("\5U+\2\u0166N\3\2\2\2\u0167\u0168\t\2\2\2\u0168P\3\2\2")
        buf.write("\2\u0169\u016a\t\3\2\2\u016aR\3\2\2\2\u016b\u016c\t\4")
        buf.write("\2\2\u016cT\3\2\2\2\u016d\u016e\t\5\2\2\u016eV\3\2\2\2")
        buf.write("\u016f\u0170\t\6\2\2\u0170X\3\2\2\2\u0171\u0172\t\7\2")
        buf.write("\2\u0172Z\3\2\2\2\u0173\u0174\t\b\2\2\u0174\\\3\2\2\2")
        buf.write("\u0175\u0176\t\t\2\2\u0176^\3\2\2\2\u0177\u0178\t\n\2")
        buf.write("\2\u0178`\3\2\2\2\u0179\u017a\t\13\2\2\u017ab\3\2\2\2")
        buf.write("\u017b\u017c\t\f\2\2\u017cd\3\2\2\2\u017d\u017e\t\r\2")
        buf.write("\2\u017ef\3\2\2\2\u017f\u0180\t\16\2\2\u0180h\3\2\2\2")
        buf.write("\u0181\u0182\t\17\2\2\u0182j\3\2\2\2\u0183\u0184\t\20")
        buf.write("\2\2\u0184l\3\2\2\2\u0185\u0186\t\21\2\2\u0186n\3\2\2")
        buf.write("\2\u0187\u0188\t\22\2\2\u0188p\3\2\2\2\u0189\u018a\t\23")
        buf.write("\2\2\u018ar\3\2\2\2\u018b\u018c\t\24\2\2\u018ct\3\2\2")
        buf.write("\2\u018d\u018e\t\25\2\2\u018ev\3\2\2\2\u018f\u0190\t\26")
        buf.write("\2\2\u0190x\3\2\2\2\u0191\u0192\t\27\2\2\u0192z\3\2\2")
        buf.write("\2\u0193\u0194\t\30\2\2\u0194|\3\2\2\2\u0195\u0196\t\31")
        buf.write("\2\2\u0196~\3\2\2\2\u0197\u0198\t\32\2\2\u0198\u0080\3")
        buf.write("\2\2\2\u0199\u019a\t\33\2\2\u019a\u0082\3\2\2\2\u019b")
        buf.write("\u019c\7-\2\2\u019c\u0084\3\2\2\2\u019d\u019e\7/\2\2\u019e")
        buf.write("\u0086\3\2\2\2\u019f\u01a0\7\61\2\2\u01a0\u0088\3\2\2")
        buf.write("\2\u01a1\u01a2\7,\2\2\u01a2\u008a\3\2\2\2\u01a3\u01a4")
        buf.write("\7?\2\2\u01a4\u008c\3\2\2\2\u01a5\u01a6\7@\2\2\u01a6\u008e")
        buf.write("\3\2\2\2\u01a7\u01a8\7@\2\2\u01a8\u01a9\7?\2\2\u01a9\u0090")
        buf.write("\3\2\2\2\u01aa\u01ab\7>\2\2\u01ab\u01ac\7@\2\2\u01ac\u0092")
        buf.write("\3\2\2\2\u01ad\u01ae\7>\2\2\u01ae\u0094\3\2\2\2\u01af")
        buf.write("\u01b0\7>\2\2\u01b0\u01b1\7?\2\2\u01b1\u0096\3\2\2\2\u01b2")
        buf.write("\u01b4\5\u00a1Q\2\u01b3\u01b2\3\2\2\2\u01b4\u01b5\3\2")
        buf.write("\2\2\u01b5\u01b3\3\2\2\2\u01b5\u01b6\3\2\2\2\u01b6\u0098")
        buf.write("\3\2\2\2\u01b7\u01b9\5\u00a1Q\2\u01b8\u01b7\3\2\2\2\u01b9")
        buf.write("\u01ba\3\2\2\2\u01ba\u01b8\3\2\2\2\u01ba\u01bb\3\2\2\2")
        buf.write("\u01bb\u01c7\3\2\2\2\u01bc\u01be\7\60\2\2\u01bd\u01bf")
        buf.write("\5\u00a1Q\2\u01be\u01bd\3\2\2\2\u01bf\u01c0\3\2\2\2\u01c0")
        buf.write("\u01be\3\2\2\2\u01c0\u01c1\3\2\2\2\u01c1\u01c3\3\2\2\2")
        buf.write("\u01c2\u01c4\5\u00a3R\2\u01c3\u01c2\3\2\2\2\u01c3\u01c4")
        buf.write("\3\2\2\2\u01c4\u01c8\3\2\2\2\u01c5\u01c8\7\60\2\2\u01c6")
        buf.write("\u01c8\5\u00a3R\2\u01c7\u01bc\3\2\2\2\u01c7\u01c5\3\2")
        buf.write("\2\2\u01c7\u01c6\3\2\2\2\u01c8\u01d3\3\2\2\2\u01c9\u01cb")
        buf.write("\7\60\2\2\u01ca\u01cc\5\u00a1Q\2\u01cb\u01ca\3\2\2\2\u01cc")
        buf.write("\u01cd\3\2\2\2\u01cd\u01cb\3\2\2\2\u01cd\u01ce\3\2\2\2")
        buf.write("\u01ce\u01d0\3\2\2\2\u01cf\u01d1\5\u00a3R\2\u01d0\u01cf")
        buf.write("\3\2\2\2\u01d0\u01d1\3\2\2\2\u01d1\u01d3\3\2\2\2\u01d2")
        buf.write("\u01b8\3\2\2\2\u01d2\u01c9\3\2\2\2\u01d3\u009a\3\2\2\2")
        buf.write("\u01d4\u01d8\7$\2\2\u01d5\u01d7\13\2\2\2\u01d6\u01d5\3")
        buf.write("\2\2\2\u01d7\u01da\3\2\2\2\u01d8\u01d9\3\2\2\2\u01d8\u01d6")
        buf.write("\3\2\2\2\u01d9\u01db\3\2\2\2\u01da\u01d8\3\2\2\2\u01db")
        buf.write("\u01dc\7$\2\2\u01dc\u009c\3\2\2\2\u01dd\u01e2\5\u009f")
        buf.write("P\2\u01de\u01e1\5\u00a1Q\2\u01df\u01e1\5\u009fP\2\u01e0")
        buf.write("\u01de\3\2\2\2\u01e0\u01df\3\2\2\2\u01e1\u01e4\3\2\2\2")
        buf.write("\u01e2\u01e0\3\2\2\2\u01e2\u01e3\3\2\2\2\u01e3\u009e\3")
        buf.write("\2\2\2\u01e4\u01e2\3\2\2\2\u01e5\u01e6\t\34\2\2\u01e6")
        buf.write("\u00a0\3\2\2\2\u01e7\u01e8\t\35\2\2\u01e8\u00a2\3\2\2")
        buf.write("\2\u01e9\u01eb\t\6\2\2\u01ea\u01ec\t\36\2\2\u01eb\u01ea")
        buf.write("\3\2\2\2\u01eb\u01ec\3\2\2\2\u01ec\u01ee\3\2\2\2\u01ed")
        buf.write("\u01ef\5\u00a1Q\2\u01ee\u01ed\3\2\2\2\u01ef\u01f0\3\2")
        buf.write("\2\2\u01f0\u01ee\3\2\2\2\u01f0\u01f1\3\2\2\2\u01f1\u00a4")
        buf.write("\3\2\2\2\u01f2\u01f3\7*\2\2\u01f3\u01f4\7,\2\2\u01f4\u01f8")
        buf.write("\3\2\2\2\u01f5\u01f7\13\2\2\2\u01f6\u01f5\3\2\2\2\u01f7")
        buf.write("\u01fa\3\2\2\2\u01f8\u01f9\3\2\2\2\u01f8\u01f6\3\2\2\2")
        buf.write("\u01f9\u01fb\3\2\2\2\u01fa\u01f8\3\2\2\2\u01fb\u01fc\7")
        buf.write(",\2\2\u01fc\u01fd\7+\2\2\u01fd\u01fe\3\2\2\2\u01fe\u01ff")
        buf.write("\bS\2\2\u01ff\u00a6\3\2\2\2\u0200\u0204\7}\2\2\u0201\u0203")
        buf.write("\13\2\2\2\u0202\u0201\3\2\2\2\u0203\u0206\3\2\2\2\u0204")
        buf.write("\u0205\3\2\2\2\u0204\u0202\3\2\2\2\u0205\u0207\3\2\2\2")
        buf.write("\u0206\u0204\3\2\2\2\u0207\u0208\7\177\2\2\u0208\u0209")
        buf.write("\3\2\2\2\u0209\u020a\bT\2\2\u020a\u00a8\3\2\2\2\u020b")
        buf.write("\u020c\7\61\2\2\u020c\u020d\7\61\2\2\u020d\u0211\3\2\2")
        buf.write("\2\u020e\u0210\n\37\2\2\u020f\u020e\3\2\2\2\u0210\u0213")
        buf.write("\3\2\2\2\u0211\u020f\3\2\2\2\u0211\u0212\3\2\2\2\u0212")
        buf.write("\u0214\3\2\2\2\u0213\u0211\3\2\2\2\u0214\u0215\bU\2\2")
        buf.write("\u0215\u00aa\3\2\2\2\u0216\u0218\t \2\2\u0217\u0216\3")
        buf.write("\2\2\2\u0218\u0219\3\2\2\2\u0219\u0217\3\2\2\2\u0219\u021a")
        buf.write("\3\2\2\2\u021a\u021b\3\2\2\2\u021b\u021c\bV\2\2\u021c")
        buf.write("\u00ac\3\2\2\2\u021d\u021e\13\2\2\2\u021e\u00ae\3\2\2")
        buf.write("\2\u021f\u0220\13\2\2\2\u0220\u00b0\3\2\2\2\u0221\u0222")
        buf.write("\13\2\2\2\u0222\u00b2\3\2\2\2\24\2\u01b5\u01ba\u01c0\u01c3")
        buf.write("\u01c7\u01cd\u01d0\u01d2\u01d8\u01e0\u01e2\u01eb\u01f0")
        buf.write("\u01f8\u0204\u0211\u0219\3\b\2\2")
        return buf.getvalue()


class MPLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    VOIDTYPE = 1
    LB = 2
    RB = 3
    LSB = 4
    RSB = 5
    COLON = 6
    SEMI = 7
    DDOT = 8
    COMMA = 9
    BREAK = 10
    CONTINUE = 11
    FOR = 12
    TO = 13
    DOWNTO = 14
    DO = 15
    IF = 16
    THEN = 17
    ELSE = 18
    RETURN = 19
    WHILE = 20
    BEGIN = 21
    END = 22
    FUNCTION = 23
    PROCEDURE = 24
    VAR = 25
    TRUE = 26
    FALSE = 27
    ARRAY = 28
    OF = 29
    REAL = 30
    BOOLEAN = 31
    INTEGER = 32
    STRING = 33
    NOT = 34
    AND = 35
    OR = 36
    DIV = 37
    MOD = 38
    ADD = 39
    SUB = 40
    DIVI = 41
    MULTI = 42
    EQUAL = 43
    GREATTHAN = 44
    GREATEQL = 45
    NOTEQL = 46
    LESSTHAN = 47
    LESSEQL = 48
    INTLIT = 49
    FLOATLIT = 50
    STRLIT = 51
    IDENT = 52
    COMMENT_1 = 53
    COMMENT_2 = 54
    COMMENT_3 = 55
    WS = 56
    ERROR_CHAR = 57
    UNCLOSE_STRING = 58
    ILLEGAL_ESCAPE = 59

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'('", "')'", "'['", "']'", "':'", "';'", "'..'", "','", "'+'", 
            "'-'", "'/'", "'*'", "'='", "'>'", "'>='", "'<>'", "'<'", "'<='" ]

    symbolicNames = [ "<INVALID>",
            "VOIDTYPE", "LB", "RB", "LSB", "RSB", "COLON", "SEMI", "DDOT", 
            "COMMA", "BREAK", "CONTINUE", "FOR", "TO", "DOWNTO", "DO", "IF", 
            "THEN", "ELSE", "RETURN", "WHILE", "BEGIN", "END", "FUNCTION", 
            "PROCEDURE", "VAR", "TRUE", "FALSE", "ARRAY", "OF", "REAL", 
            "BOOLEAN", "INTEGER", "STRING", "NOT", "AND", "OR", "DIV", "MOD", 
            "ADD", "SUB", "DIVI", "MULTI", "EQUAL", "GREATTHAN", "GREATEQL", 
            "NOTEQL", "LESSTHAN", "LESSEQL", "INTLIT", "FLOATLIT", "STRLIT", 
            "IDENT", "COMMENT_1", "COMMENT_2", "COMMENT_3", "WS", "ERROR_CHAR", 
            "UNCLOSE_STRING", "ILLEGAL_ESCAPE" ]

    ruleNames = [ "VOIDTYPE", "LB", "RB", "LSB", "RSB", "COLON", "SEMI", 
                  "DDOT", "COMMA", "BREAK", "CONTINUE", "FOR", "TO", "DOWNTO", 
                  "DO", "IF", "THEN", "ELSE", "RETURN", "WHILE", "BEGIN", 
                  "END", "FUNCTION", "PROCEDURE", "VAR", "TRUE", "FALSE", 
                  "ARRAY", "OF", "REAL", "BOOLEAN", "INTEGER", "STRING", 
                  "NOT", "AND", "OR", "DIV", "MOD", "A", "B", "C", "D", 
                  "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", 
                  "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", 
                  "ADD", "SUB", "DIVI", "MULTI", "EQUAL", "GREATTHAN", "GREATEQL", 
                  "NOTEQL", "LESSTHAN", "LESSEQL", "INTLIT", "FLOATLIT", 
                  "STRLIT", "IDENT", "NONDIGIT", "DIGIT", "EXPONENT", "COMMENT_1", 
                  "COMMENT_2", "COMMENT_3", "WS", "ERROR_CHAR", "UNCLOSE_STRING", 
                  "ILLEGAL_ESCAPE" ]

    grammarFileName = "MP.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


