# Generated from main/mp/parser/MP.g4 by ANTLR 4.7.1
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3=")
        buf.write("F\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b")
        buf.write("\t\b\4\t\t\t\4\n\t\n\3\2\6\2\26\n\2\r\2\16\2\27\3\3\3")
        buf.write("\3\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\7\5%\n\5\f\5\16")
        buf.write("\5(\13\5\3\6\3\6\3\6\5\6-\n\6\3\7\3\7\3\7\3\b\3\b\5\b")
        buf.write("\64\n\b\3\t\3\t\3\t\5\t9\n\t\3\t\3\t\3\n\3\n\3\n\3\n\3")
        buf.write("\n\3\n\3\n\3\n\3\n\3\n\2\2\13\2\4\6\b\n\f\16\20\22\2\3")
        buf.write("\4\2  \"\"\2B\2\25\3\2\2\2\4\31\3\2\2\2\6\33\3\2\2\2\b")
        buf.write("!\3\2\2\2\n,\3\2\2\2\f.\3\2\2\2\16\63\3\2\2\2\20\65\3")
        buf.write("\2\2\2\22<\3\2\2\2\24\26\5\4\3\2\25\24\3\2\2\2\26\27\3")
        buf.write("\2\2\2\27\25\3\2\2\2\27\30\3\2\2\2\30\3\3\2\2\2\31\32")
        buf.write("\5\6\4\2\32\5\3\2\2\2\33\34\7\33\2\2\34\35\5\b\5\2\35")
        buf.write("\36\7\b\2\2\36\37\5\n\6\2\37 \7\t\2\2 \7\3\2\2\2!&\7\66")
        buf.write("\2\2\"#\7\13\2\2#%\7\66\2\2$\"\3\2\2\2%(\3\2\2\2&$\3\2")
        buf.write("\2\2&\'\3\2\2\2\'\t\3\2\2\2(&\3\2\2\2)-\7\"\2\2*-\7 \2")
        buf.write("\2+-\5\22\n\2,)\3\2\2\2,*\3\2\2\2,+\3\2\2\2-\13\3\2\2")
        buf.write("\2./\5\20\t\2/\60\7\t\2\2\60\r\3\2\2\2\61\64\5\20\t\2")
        buf.write("\62\64\7\63\2\2\63\61\3\2\2\2\63\62\3\2\2\2\64\17\3\2")
        buf.write("\2\2\65\66\7\66\2\2\668\7\4\2\2\679\5\16\b\28\67\3\2\2")
        buf.write("\289\3\2\2\29:\3\2\2\2:;\7\5\2\2;\21\3\2\2\2<=\7\36\2")
        buf.write("\2=>\7\6\2\2>?\7\63\2\2?@\7\n\2\2@A\7\63\2\2AB\7\7\2\2")
        buf.write("BC\7\37\2\2CD\t\2\2\2D\23\3\2\2\2\7\27&,\638")
        return buf.getvalue()


class MPParser ( Parser ):

    grammarFileName = "MP.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "<INVALID>", "'('", "')'", "'['", "']'", 
                     "':'", "';'", "'..'", "','", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "'+'", "'-'", 
                     "'/'", "'*'", "'='", "'>'", "'>='", "'<>'", "'<'", 
                     "'<='" ]

    symbolicNames = [ "<INVALID>", "VOIDTYPE", "LB", "RB", "LSB", "RSB", 
                      "COLON", "SEMI", "DDOT", "COMMA", "BREAK", "CONTINUE", 
                      "FOR", "TO", "DOWNTO", "DO", "IF", "THEN", "ELSE", 
                      "RETURN", "WHILE", "BEGIN", "END", "FUNCTION", "PROCEDURE", 
                      "VAR", "TRUE", "FALSE", "ARRAY", "OF", "REAL", "BOOLEAN", 
                      "INTEGER", "STRING", "NOT", "AND", "OR", "DIV", "MOD", 
                      "ADD", "SUB", "DIVI", "MULTI", "EQUAL", "GREATTHAN", 
                      "GREATEQL", "NOTEQL", "LESSTHAN", "LESSEQL", "INTLIT", 
                      "FLOATLIT", "STRLIT", "IDENT", "COMMENT_1", "COMMENT_2", 
                      "COMMENT_3", "WS", "ERROR_CHAR", "UNCLOSE_STRING", 
                      "ILLEGAL_ESCAPE" ]

    RULE_program = 0
    RULE_declaration = 1
    RULE_vardecl = 2
    RULE_idlist = 3
    RULE_mptype = 4
    RULE_body = 5
    RULE_exp = 6
    RULE_funcall = 7
    RULE_arraytype = 8

    ruleNames =  [ "program", "declaration", "vardecl", "idlist", "mptype", 
                   "body", "exp", "funcall", "arraytype" ]

    EOF = Token.EOF
    VOIDTYPE=1
    LB=2
    RB=3
    LSB=4
    RSB=5
    COLON=6
    SEMI=7
    DDOT=8
    COMMA=9
    BREAK=10
    CONTINUE=11
    FOR=12
    TO=13
    DOWNTO=14
    DO=15
    IF=16
    THEN=17
    ELSE=18
    RETURN=19
    WHILE=20
    BEGIN=21
    END=22
    FUNCTION=23
    PROCEDURE=24
    VAR=25
    TRUE=26
    FALSE=27
    ARRAY=28
    OF=29
    REAL=30
    BOOLEAN=31
    INTEGER=32
    STRING=33
    NOT=34
    AND=35
    OR=36
    DIV=37
    MOD=38
    ADD=39
    SUB=40
    DIVI=41
    MULTI=42
    EQUAL=43
    GREATTHAN=44
    GREATEQL=45
    NOTEQL=46
    LESSTHAN=47
    LESSEQL=48
    INTLIT=49
    FLOATLIT=50
    STRLIT=51
    IDENT=52
    COMMENT_1=53
    COMMENT_2=54
    COMMENT_3=55
    WS=56
    ERROR_CHAR=57
    UNCLOSE_STRING=58
    ILLEGAL_ESCAPE=59

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class ProgramContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def declaration(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MPParser.DeclarationContext)
            else:
                return self.getTypedRuleContext(MPParser.DeclarationContext,i)


        def getRuleIndex(self):
            return MPParser.RULE_program

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProgram" ):
                return visitor.visitProgram(self)
            else:
                return visitor.visitChildren(self)




    def program(self):

        localctx = MPParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_program)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 19 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 18
                self.declaration()
                self.state = 21 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==MPParser.VAR):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class DeclarationContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def vardecl(self):
            return self.getTypedRuleContext(MPParser.VardeclContext,0)


        def getRuleIndex(self):
            return MPParser.RULE_declaration

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDeclaration" ):
                return visitor.visitDeclaration(self)
            else:
                return visitor.visitChildren(self)




    def declaration(self):

        localctx = MPParser.DeclarationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_declaration)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 23
            self.vardecl()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class VardeclContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def VAR(self):
            return self.getToken(MPParser.VAR, 0)

        def idlist(self):
            return self.getTypedRuleContext(MPParser.IdlistContext,0)


        def COLON(self):
            return self.getToken(MPParser.COLON, 0)

        def mptype(self):
            return self.getTypedRuleContext(MPParser.MptypeContext,0)


        def SEMI(self):
            return self.getToken(MPParser.SEMI, 0)

        def getRuleIndex(self):
            return MPParser.RULE_vardecl

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVardecl" ):
                return visitor.visitVardecl(self)
            else:
                return visitor.visitChildren(self)




    def vardecl(self):

        localctx = MPParser.VardeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_vardecl)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 25
            self.match(MPParser.VAR)
            self.state = 26
            self.idlist()
            self.state = 27
            self.match(MPParser.COLON)
            self.state = 28
            self.mptype()
            self.state = 29
            self.match(MPParser.SEMI)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class IdlistContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IDENT(self, i:int=None):
            if i is None:
                return self.getTokens(MPParser.IDENT)
            else:
                return self.getToken(MPParser.IDENT, i)

        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(MPParser.COMMA)
            else:
                return self.getToken(MPParser.COMMA, i)

        def getRuleIndex(self):
            return MPParser.RULE_idlist

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIdlist" ):
                return visitor.visitIdlist(self)
            else:
                return visitor.visitChildren(self)




    def idlist(self):

        localctx = MPParser.IdlistContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_idlist)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 31
            self.match(MPParser.IDENT)
            self.state = 36
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==MPParser.COMMA:
                self.state = 32
                self.match(MPParser.COMMA)
                self.state = 33
                self.match(MPParser.IDENT)
                self.state = 38
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class MptypeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INTEGER(self):
            return self.getToken(MPParser.INTEGER, 0)

        def REAL(self):
            return self.getToken(MPParser.REAL, 0)

        def arraytype(self):
            return self.getTypedRuleContext(MPParser.ArraytypeContext,0)


        def getRuleIndex(self):
            return MPParser.RULE_mptype

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMptype" ):
                return visitor.visitMptype(self)
            else:
                return visitor.visitChildren(self)




    def mptype(self):

        localctx = MPParser.MptypeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_mptype)
        try:
            self.state = 42
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [MPParser.INTEGER]:
                self.enterOuterAlt(localctx, 1)
                self.state = 39
                self.match(MPParser.INTEGER)
                pass
            elif token in [MPParser.REAL]:
                self.enterOuterAlt(localctx, 2)
                self.state = 40
                self.match(MPParser.REAL)
                pass
            elif token in [MPParser.ARRAY]:
                self.enterOuterAlt(localctx, 3)
                self.state = 41
                self.arraytype()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class BodyContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def funcall(self):
            return self.getTypedRuleContext(MPParser.FuncallContext,0)


        def SEMI(self):
            return self.getToken(MPParser.SEMI, 0)

        def getRuleIndex(self):
            return MPParser.RULE_body

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBody" ):
                return visitor.visitBody(self)
            else:
                return visitor.visitChildren(self)




    def body(self):

        localctx = MPParser.BodyContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_body)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 44
            self.funcall()
            self.state = 45
            self.match(MPParser.SEMI)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ExpContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def funcall(self):
            return self.getTypedRuleContext(MPParser.FuncallContext,0)


        def INTLIT(self):
            return self.getToken(MPParser.INTLIT, 0)

        def getRuleIndex(self):
            return MPParser.RULE_exp

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp" ):
                return visitor.visitExp(self)
            else:
                return visitor.visitChildren(self)




    def exp(self):

        localctx = MPParser.ExpContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_exp)
        try:
            self.state = 49
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [MPParser.IDENT]:
                self.enterOuterAlt(localctx, 1)
                self.state = 47
                self.funcall()
                pass
            elif token in [MPParser.INTLIT]:
                self.enterOuterAlt(localctx, 2)
                self.state = 48
                self.match(MPParser.INTLIT)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class FuncallContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IDENT(self):
            return self.getToken(MPParser.IDENT, 0)

        def LB(self):
            return self.getToken(MPParser.LB, 0)

        def RB(self):
            return self.getToken(MPParser.RB, 0)

        def exp(self):
            return self.getTypedRuleContext(MPParser.ExpContext,0)


        def getRuleIndex(self):
            return MPParser.RULE_funcall

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFuncall" ):
                return visitor.visitFuncall(self)
            else:
                return visitor.visitChildren(self)




    def funcall(self):

        localctx = MPParser.FuncallContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_funcall)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 51
            self.match(MPParser.IDENT)
            self.state = 52
            self.match(MPParser.LB)
            self.state = 54
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==MPParser.INTLIT or _la==MPParser.IDENT:
                self.state = 53
                self.exp()


            self.state = 56
            self.match(MPParser.RB)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ArraytypeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ARRAY(self):
            return self.getToken(MPParser.ARRAY, 0)

        def LSB(self):
            return self.getToken(MPParser.LSB, 0)

        def INTLIT(self, i:int=None):
            if i is None:
                return self.getTokens(MPParser.INTLIT)
            else:
                return self.getToken(MPParser.INTLIT, i)

        def DDOT(self):
            return self.getToken(MPParser.DDOT, 0)

        def RSB(self):
            return self.getToken(MPParser.RSB, 0)

        def OF(self):
            return self.getToken(MPParser.OF, 0)

        def INTEGER(self):
            return self.getToken(MPParser.INTEGER, 0)

        def REAL(self):
            return self.getToken(MPParser.REAL, 0)

        def getRuleIndex(self):
            return MPParser.RULE_arraytype

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitArraytype" ):
                return visitor.visitArraytype(self)
            else:
                return visitor.visitChildren(self)




    def arraytype(self):

        localctx = MPParser.ArraytypeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_arraytype)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 58
            self.match(MPParser.ARRAY)
            self.state = 59
            self.match(MPParser.LSB)
            self.state = 60
            self.match(MPParser.INTLIT)
            self.state = 61
            self.match(MPParser.DDOT)
            self.state = 62
            self.match(MPParser.INTLIT)
            self.state = 63
            self.match(MPParser.RSB)
            self.state = 64
            self.match(MPParser.OF)
            self.state = 65
            _la = self._input.LA(1)
            if not(_la==MPParser.REAL or _la==MPParser.INTEGER):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





