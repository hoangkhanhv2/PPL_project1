import unittest
from TestUtils import TestLexer

class LexerSuite(unittest.TestCase):
      
    def test_identifier(self):
        """test identifiers"""
        self.assertTrue(TestLexer.test("_","_,<EOF>",101))
        self.assertTrue(TestLexer.test("_aCBbdc","_aCBbdc,<EOF>",102))
        self.assertTrue(TestLexer.test("aA9VN_","aA9VN_,<EOF>",103))

    def test_integer(self):
        """test integers"""
        self.assertTrue(TestLexer.test("123a123","123,a123,<EOF>",104))

    def test_comment(self): 
        """ test comments"""
        self.assertTrue(TestLexer.test("(* This is a comments *)", "<EOF>",105))
        self.assertTrue(TestLexer.test("{ This is a \r \n comments }", "<EOF>", 108))
        self.assertTrue(TestLexer.test("// This is a \n comments", "comments,<EOF>", 109))
    def test_string(self):
        self.assertTrue(TestLexer.test("\"This\n \\is\b a test \"", "\"This\n \\is\b a test \",<EOF>", 110)) 
    def test_float(self):
        self.assertTrue(TestLexer.test("20e+10", "20e+10,<EOF>", 111))
        self.assertTrue(TestLexer.test("1.", "1.,<EOF>", 112))  
        self.assertTrue(TestLexer.test(".1", ".1,<EOF>", 113))  
        self.assertTrue(TestLexer.test(".1e-10", ".1e-10,<EOF>", 114))  
    def test_key(self):
        self.assertTrue(TestLexer.test("function adf(a : integer): real; var x: integer;begin end", "reTurn,<EOF>", 115))
        

