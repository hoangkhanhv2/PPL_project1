grammar MP;

@lexer::header {
from lexererr import *
}

options{
	language=Python3;
}
body: funcall SEMI;
exp: funcall | INTLIT ;
funcall: IDENT LB exp? RB ;

program: declaration+ ;

declaration: vardecl | funcdecl ;

vardecl: VAR (paradecl SEMI)+;

funcdecl: fparadecl vardecl compsmt;

fparadecl: FUNCTION IDENT LB paralist RB COLON mptype SEMI;

paralist: ( paradecl | (SEMI paradecl ) * ) ?;

paradecl: idlist COLON mptype;

/*Come here*/
compsmt: BEGIN  END;

idlist: IDENT (COMMA IDENT)*;

mptype: primitype | arraytype ;

primitype: INTEGER | REAL | STRING | BOOLEAN;

VOIDTYPE: V O I D;

arraytype: ARRAY LSB INTLIT DDOT INTLIT RSB OF primitype;

LB: '(' ;

RB: ')' ;

LSB: '[';

RSB: ']';

COLON: ':';

SEMI: ';' ;

DDOT: '..';

COMMA: ',';

BREAK: B R E A K;

CONTINUE: C O N T I N U E ;

FOR: F O R;

TO: T O;

DOWNTO: D O W N T O;

DO: D O;

IF: I F;

THEN: T H E N;

ELSE: E L S E;

RETURN: R E T U R N;

WHILE: W H I L E;

BEGIN: B E G I N;

END: E N D;

FUNCTION: F U N C T I O N;

PROCEDURE: P R O C E D U R E;

VAR: V A R;

TRUE: T R U E;

FALSE: F A L S E;

ARRAY: A R R A Y;

OF: O F;

REAL: R E A L;

BOOLEAN: B O O L E A N;

INTEGER: I N T E G E R;

STRING: S T R I N G;

NOT: N O T;

AND: A N D;

OR: O R;

DIV: D I V;

MOD: M O D;

fragment A: ('a' | 'A');

fragment B: ('b' | 'B');

fragment C: ('c' | 'C');

fragment D: ('d' | 'D');

fragment E: ('e' | 'E');

fragment F: ('f' | 'F');

fragment G: ('g' | 'G');

fragment H: ('h' | 'H');

fragment I: ('i' | 'I');

fragment J: ('j' | 'J');

fragment K: ('k' | 'K');

fragment L: ('l' | 'L');

fragment M: ('m' | 'M');

fragment N: ('n' | 'N');

fragment O: ('o' | 'O');

fragment P: ('p' | 'P');

fragment Q: ('q' | 'Q');

fragment R: ('r' | 'R');

fragment S: ('s' | 'S');

fragment T: ('t' | 'T');

fragment U: ('u' | 'U');

fragment V: ('v' | 'V');

fragment W: ('w' | 'W');

fragment X: ('x' | 'X');

fragment Y: ('y' | 'Y');

fragment Z: ('z' | 'Z');

ADD: '+';

SUB: '-';

DIVI: '/';

MULTI: '*';

EQUAL: '=';

GREATTHAN: '>';

GREATEQL: '>=';

NOTEQL: '<>';

LESSTHAN: '<';

LESSEQL: '<=';

INTLIT: DIGIT+; 

FLOATLIT: (DIGIT+ ('.' DIGIT+ EXPONENT? | '.' | EXPONENT)) | ('.' DIGIT+ EXPONENT?) ;

STRLIT: '"' .*? '"';

IDENT: NONDIGIT (DIGIT | NONDIGIT)* ;

fragment NONDIGIT: [_a-zA-Z] ;

fragment DIGIT: [0-9];

fragment EXPONENT: ('e'|'E') ('+'|'-')? DIGIT+;

COMMENT_1: '(*' .*? '*)' -> skip;

COMMENT_2: '{' .*? '}' -> skip;

COMMENT_3: '//' ~[\r\n]* -> skip;

WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines

ERROR_CHAR: .;
UNCLOSE_STRING: .;
ILLEGAL_ESCAPE: .;